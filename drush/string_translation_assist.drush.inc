<?php


/**
 * Implements hook_drush_command().
 */
function string_translation_assist_drush_command() {
  $items['str-translate'] = array(
    'description' => dt('Gather and translate new & empty-translation strings'),
    'aliases' => array('sta-t'),
  );
  $items['str-translate-search'] = array(
    'description' => dt('Find source (english) strings by pattern'),
    'aliases' => array('sta-s'),
    'required-arguments' => 1,
    'arguments' => [
      'pattern' => dt('Pattern to use for searching source strings')
    ],
    'options' => [
      'language' => dt('Show only those translated in this language')
    ]
  );
  $items['str-translate-up'] = array(
    'description' => dt('Update existing translation for a string'),
    'aliases' => array('sta-up'),
    'required-arguments' => 3,
    'arguments' => [
      'lid' => dt('Locale ID of the existing string in english (use sta-s to search)'),
      'langcode' => dt('Language to translate to'),
      'value' => dt('New value')
    ],
    'options' => [
      'language' => dt('Show only those translated in this language')
    ]
  );
  return $items;
}


function drush_string_translation_assist_str_translate() {
  $assistant = new \Drupal\string_translation_assist\StringTranslationAssist();
  $languages = \Drupal::languageManager()->getLanguages();
  unset($languages['en']);
  $changed = FALSE;
  foreach ($languages as $langcode => $language) {
    $status = ['@langcode' => $langcode, '@new' => 0, '@updated' => 0, '@unchanged' => 0];
    $strings = $assistant->getStrings($langcode);
    /** @var \Drupal\locale\LocaleConfigManager $locale */
    foreach ($strings as $english => $translation) {
      $result = $assistant->translateString($english, $translation, $langcode);
      switch ($result) {
        case \Drupal\string_translation_assist\StringTranslationAssist::STRING_CREATED:
          $status['@new'] += 1;
          $changed = TRUE;
          break;

        case \Drupal\string_translation_assist\StringTranslationAssist::STRING_UPDATED:
          $status['@updated'] += 1;
          $changed = TRUE;
          break;

        default:
          $status['@unchanged'] += 1;
          break;
      }
    }
    drush_log(dt('Finshed translating \'@langcode\' language: new=@new, updated=@updated, unchanged=@unchanged', $status), 'ok');
  }
  if ($changed) {
    \Drupal::cache()->deleteAll();
  }
}


function drush_string_translation_assist_str_translate_search() {
  $assistant = new \Drupal\string_translation_assist\StringTranslationAssist();
  $args = drush_get_arguments();
  $language = drush_get_option('language');
  $verbose = drush_get_context('DRUSH_VERBOSE');
  $rows = $assistant->findSourceString($args[1], $language);
  /** @var \Drupal\locale\SourceString $row */
  foreach($rows as $row) {
    if ($verbose) {
      $output = [sprintf("lid: %7s: %s", $row->getId(), $row->getString())];
      $translations = $assistant->getTranslations($row->getId(), $language);
      /** @var \Drupal\locale\TranslationString $t */
      foreach ($translations as $t) {
        $output[] = sprintf("  %s: %s", $t->language, $t->getString());
      }
      drush_log(implode("\n", $output), 'ok');
    } else {
      drush_log(sprintf('lid: %7s: %s', $row->getId(), $row->getString()), 'ok');
    }
  }
}

function drush_string_translation_assist_str_translate_up() {
  $assistant = new \Drupal\string_translation_assist\StringTranslationAssist();
  $args = drush_get_arguments();
  $lid = $args[1];
  $langcode = $args[2];
  $value = $args[3];
  $existing = \Drupal::languageManager()->getLanguages();
  if (!array_key_exists($langcode, $existing)) {
    drush_set_error('string_translation_assist', sprintf('Language %s is not configured in this instance', $langcode));
    return;
  }
  if ($assistant->setTranslation($lid, $langcode, $value) == \Drupal\string_translation_assist\StringTranslationAssist::STRING_UPDATED) {
    drush_log(sprintf('Translation updated for language: %s', $langcode), 'ok');
  }
  else {
    drush_log(sprintf('Translation created for language: %s', $langcode), 'ok');
  }
  \Drupal::cache()->deleteAll();
}
