<?php
/**
 * @file
 * Contains Drupal\string_translation_assist\StringTranslationAssist.
 */

namespace Drupal\string_translation_assist;

use Drupal\locale\SourceString;
use Drupal\locale\StringDatabaseStorage;

/**
 * Class StringTranslationAssist provides helper to translate custom strings
 * provided by custom modules.
 *
 * @package Drupal\string_translation_assist
 */
class StringTranslationAssist {

  /** Translation was updated */
  const STRING_UPDATED = 1;
  /** New translation was created */
  const STRING_CREATED = 2;

  /**
   * @return StringDatabaseStorage
   */
  protected function getStorage() {
    return \Drupal::service('locale.storage');
  }

  public function getStrings($langcode) {
    $args = ['langcode' => $langcode];
    $strings = \Drupal::moduleHandler()->invokeAll('string_translation_assist_source', $args);
    \Drupal::moduleHandler()->alter('string_translation_assist_source', $strings, $args);
    return $strings;
  }

  /**
   * Translate source strings from English to custom language.
   *
   * @param string $source
   *   Source string in English
   * @param string $translation
   *   Translation in the specified language
   * @param string $langcode
   *   The language code to translate into
   *
   * @return int
   *   String was created, changed or left untouched (FALSE)
   */
  public function translateString($source, $translation, $langcode) {
    $ret = 0;
    $context = !empty($translation['context']) ?: '';
    $translated = !empty($translation['translation']) ? $translation['translation'] : $translation;
    $storage = $this->getStorage();
    if ($available = $this->getStorage()->getTranslations(['source' => $source, 'context' => $context])) {
      /** @var \Drupal\locale\TranslationString $tr */
      foreach ($available as $tr) {
        // if ($tr->getString() != $translated)
        // @todo Translate only empty strings, some could be overridden from UI!
        if (empty($tr->getString())) {
          $storage->createTranslation([
            'lid' => $tr->getId(),
            'language' => $langcode
          ])->setString($translated)->save();
          $ret = self::STRING_UPDATED;
        }
      }
    }
    else {
      /** @var SourceString $ob */
      $ob = $storage->createString(['source' => $source, 'context' => $context]);
      $ob->save();
      $storage->createTranslation(['lid' => $ob->getId(), 'language' => $langcode])
        ->setString($translated)
        ->setCustomized()
        ->save();
      $ret = self::STRING_CREATED;
    }
    return $ret;
  }

  /**
   * Find source string by search pattern.
   *
   * @param string $pattern
   *   Search pattern
   * @param string $language
   *   Optional language code to filter only specific language
   *
   * @return array|\Drupal\locale\StringInterface[]
   *   Array of objects
   */
  public function findSourceString($pattern, $language = NULL) {
    /** @var StringDatabaseStorage $storage */
    $storage = $this->getStorage();
    $conditions = [];
    $filter_options = ['filters' => ['source' => $pattern]];
    if (!empty($language)) {
      $conditions['language'] = $language;
      $conditions['translated'] = TRUE;
    }
    return $storage->getStrings($conditions, $filter_options);
  }

  /**
   * Get item translations.
   *
   * @param integer $lid
   *   Item ID
   * @param string $language
   *   Filter by specific language only
   * @return array|\Drupal\locale\StringInterface[]
   *   Array of objects
   */
  public function getTranslations($lid, $language = NULL) {
    $q = ['lid' => $lid];
    if ($language) {
      $q['language'] = $language;
    }
    return $this->getStorage()->getTranslations($q);
  }

  /**
   * Set/change translation for a string for a specific language.
   *
   * @param int $lid
   *   Item ID
   * @param string $language
   *   Language code
   * @param string $value
   *   New translation to set
   * @param string $context
   *   Translation context (unused).
   *
   * @return
   *   String was created, changed or left untouched (FALSE)
   */
  public function setTranslation($lid, $language, $value, $context = NULL) {
    /** @var StringDatabaseStorage $storage */
    $storage = $this->getStorage();
    $q = ['lid' => $lid, 'language' => $language];
    if ($context) {
      $q['context'] = $context;
    }
    $existing = $storage->getTranslations($q);
    if (!empty($existing) && $ob = $existing[0]) {
      $ob->setString($value)->save();
      return self::STRING_UPDATED;
    }
    else {
      $storage->createTranslation([
        'lid' => $lid,
        'language' => $language
      ])->setString($value)->save();
      return self::STRING_CREATED;
    }
  }
}
