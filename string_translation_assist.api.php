<?php

/**
 * Provide the strings to translate for the translation assistant.
 *
 * @param string $langcode
 *   The language code to provide translations for
 *
 * @return array
 *   Array keyed by English string, while the value can be either the
 *   actual translation or an array with 'translation' and 'context', if any.
 *
 */
function hook_string_translation_assist_source($langcode) {
  if ($langcode == 'ro') {
    return [
      'Custom message' => 'Mesaj personalizat',
      'Contextual message' => [
        'translation' => 'Mesaj contextual',
        'context' => 'custom_contet'
      ]
    ];
  }
  if ($langcode == 'fr') {
    return [
      'Custom message' => 'Message personnalisé'
    ];
  }
  return [];
}


/**
 * Alter the translations provided by other modules.
 *
 * @see hook_string_translation_assist_source()
 */
function hook_string_translation_assist_source_alter(&$data, $langcode) {
  if ($langcode == 'ro') {
    $data['Custom message'] = 'Mesaj custom';
  }
}
